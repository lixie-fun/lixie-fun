**Content:**  

[[_TOC_]]

# Content of this directory
* **[doc](Hardware/numeral_plates/doc)**  
  Schematics and images of the numeral plates
* `.ai`   
files for changing the design in Illustrator
* `.dxf`  
files for importing into the laser-cutter software

# Tools/Software
## Laser-Cutter
For the making of the number plates we use a 150W-CO2-Laser. The used Laser-Cutter is from BRM.

![Laser-Cutter](https://www.brmlasers.com/app/uploads/BRM_100160_01.png)

Supporting Documents (in German):
* [BRM-Laser Information](https://www.brmlasers.com/de/lasermaschinen/original-lasers/brm-100160/)
* [BRM-Laser Manual](uploads/5810cb7a72a0ed894279f18f3fc44375/BRMLasers-100160.pdf)
* [Supported Materials](uploads/604a8109d0a900a07609890fa03c77c4/BRMLasers-Materialien.pdf)

If you are a student at the Media University Stuttgart, you can find further information on the workflow of the HdM Laser in this document (German):
* [Workflow for HdM Lasercutter](uploads/151fe5a6d969a70cde767c90a52a5301/Anleitung_600x400mm.pdf)

**Important**: Do not use the cheap Plexiglas, also called "Bastlerglas" in Germany. It consists *polysterol*, which releases toxic gases during the engraving process. Only use real Acrylic Glass, that is made of *PMMA (Polymethyl methacrylate)*. There are two types of Acrylic Glass: XT (extruded) and GS (cast). GS is better for engraving, because it is much clearer, but the price is the double of the XT.

### RDWorks
The software used for the laser-cutter. It is being adapted for numerous manufacturers, all of which are based on Chinese laser cutters. There is a "simulation" mode in the software in which the laser process can be run in real time or in time-lapse. The program works with different layers that have different colors, which determine how a certain part of the object to be lasered is handled (i.e. if and how to cut, engrave, etc.).
Download RD-Works (incl. PDF manual) at: [RD-Works](http://www.thunderlaser.com/laser-download )

### Vector-based Design Program
For the design of the numeral panes you need a vector-based software.
The design of the number panes was done in [Illustrator](https://www.adobe.com/de/products/illustrator/free-trial-download.html?gclid=EAIaIQobChMIqubRpuWD6AIVQUPTCh3SNANREAAYASAAEgKXSPD_BwE&sdid=88X75SKP&mv=search&ef_id=EAIaIQobChMIqubRpuWD6AIVQUPTCh3SNANREAAYASAAEgKXSPD_BwE:G:s&s_kwcid=AL!3085!3!341761381160!e!!g!!illustrator%20download). As a open-source program you can use [Inkscape](https://inkscape.org/de/). 

The final design is then exported as a `.dxf` which you can import into RDWorks.  

## Measurements
10 panes for one segment: 157x245mm.  

![10panes_1layer_1segment](doc/10panes_1layer_1segment.png)
Therefore, for one whole clock (6 Segments), the acrylic sheet needs to be 335mm x 735mm.  

![10panes_1layer_6segments](doc/10panes_1layer_6segments.png)

Adding some surface around the edges to account for inaccuracies of the laser cutter, this results in an area of:  
380mm x 780mm = 0,296m². Regarding the cost of 31,90€/m² at Bauhaus, this makes an acrylic sheet for one clock cost **10,95€**

## Material-Settings
There are several different ways to create the numeral engraving on the acrylic plate. A first guide to the right settings for your chosen material can be found here:
[Material-settings.pdf](uploads/d251f5aac9726ba77e224d423c76e7f6/Schnittdaten.pdf)  

Depending on the chosen style of the lixie numeral plates, these settings need to be adjusted. 

Acrylic Glass (2mm):

* Pane Edge Cut
    * Min Power: 95%
    * Max Power: 100%
    * Speed: 40mm/s
* Number Outline Cut
    * Min Power: 30%
    * Max Power: 40%
    * Speed: 200mm/s
* Scan (filled engraving)
    * power: 50%
    * speed: 500mm

### Engraving with Scan
The numbers are engraved as a closed shape with the _Scan_ mode of the laser.  
By principle, this setting results in the protective foil of the acrylic, which is needed to prevent burning marks around the cutting edges, being burned into the surface of the numerals, leaving a white layer of residual material.  
 
This makes each number very clear, but also very opaque, which makes it hard to see the numbers further back in the stack.  

![number_scan](doc/number_scan.png)  

Cleaning up the white layer by scratching and melting off the non-acrylic material brought an improvement, but the numbers at the back are still too hard to see. A solution to this could be the use of more expensive acrylic glass.

### Outlining with reduced cutting power
This achieved much better results; the far back numbers are easily recognizable. We decided to use this method for our version of the lixie display.

![number_outline_cut](doc/number_outline_cut.png)  

## Things to note

Our design of the numeral plates has little *gaps* at the bottom of the numbers who consist of closed shapes like 0, 4, 6, 8 and 9. This helps the light of the LEDs to spread across the whole number evenly. Otherwise the top of those numbers would not get enough light and will be significantly darker when lit.

![number_gaps](doc/number_gaps.png)  

**Attention**: It is important that you **do not** try to clean the numeral plates with ethanol or any other alcoholic substance. It will corrode the acrylic glass, leaving a frayed edge around the numbers. To avoid burning marks you can leave the protective foil on the glass while using the laser but as mentioned before, this affects the result when using the *scan*-method.

![frayed_edges](doc/frayed_edges.jpg)  

Furthermore, minimal changes in the settings of the laser cutter can effect the result of the outcome.  
For example, the min power for the number outline cut on our first try was too high. In the corners, where the laser stops and changes direction, the cut went all the way through the material.  
  
![cut_too_strong](doc/cut_too_strong.png)  
  
While this is a slight imperfection, it doesn't really affect the outcome. The corners appear a little brighter when the pane is lit, but it's hardly noticable. Still, the power settings needed to be adapted for future iterations.