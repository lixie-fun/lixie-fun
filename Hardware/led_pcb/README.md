To build a Lixie-Display you need a special design of circuit boards for the LEDs.  
In this folder you will find the needed files for adjusting the design as well as the exported gerber files for ordering the pcbs directly online.

**Content:**  

[[_TOC_]]

# Content of this directory
* **[doc](Hardware/led_pcb/doc)**  
  Schematics and images of the pcb
* `.brd` + `.sch`  
files to edit the pcb in Eagle
* `.zip`   
folder for ordering the pcb

# Requirements and Implementation
Since one Display-Segment consists of 10 numeral plates each lit by two LEDs, the pcb has to have 10 rows of LED pairs. 

To design the circuit board, you can use the Autodesk Software *Eagle*, which combines drawing the circuit diagram with building the final circuit board. Further advances are the possibilities to export the circuit board directly to *Fusion 360*, where we can model our mount around it, and the export as a *gerber file*, which we need for ordering the circuit boards.

Important things when designing your own circuit board:
*  Set the right components for your design: 
    * APA102 LEDs 5x5 mm
    * Mount hole size 3.0 mm
*  The settings for spacings, sizes and diameters can be found at the website where you plan to order your pcbs from and have to be set in the DRC settings inside the Eagle Software. In our case they can be found at [Capabilities](https://jlcpcb.com/capabilities/Capabilities). 
*  Change the default settings from mil to mm.
*  Optional: Set ground copper rectangles on both sides of the circuit board.
*  Make sure all the wires are connected the right way (5V, GRD, wires between each LED).
*  You can use vias to put wires on the bottom side of the circuit board. The cost will be the same.
* Before exporting the final Design you need to do a DRC-Check. That way you make sure that all your spaces, sizes and diameters are correct.

# The Design
There are of course multiple ways to design a pcb for a lixie display. We chose this design since it has its focus on being small and compact. The LEDs are close together and it has a square shape.  

Dimensions: 67.3 x 55 mm.  

We placed pin headers on the sides of each circuit board so the display segments are modular. With the four mounting holes it is possible to make the pcb fit on our 3D printed mount nice and steady.

![circuitBoard](doc/pcb_design.png)

For having a better overview of what your design will look like before ordering, you can export `.png` that show the top and bottom side of the circuit board: 

![lixie_circuitBoard](doc/pcb_graphic.png)

# Ordering and Delivery
We decided to order the circuit boards on [jlc-pcb.com](https://jlcpcb.com/). If you chose fast delivery, the delivery takes around 5 days. The quality is quite good, even though some pcbs are more accurate than others.

To place an order you need to export the gerber files from Eagle and drop and drag the exported zip folder onto the website. After that you can modify the color, amount, size, etc.   
We recommend ordering a lot pcbs at once since the price difference in the amount is very small. Also remember to order spare boards, because it is possible that the boards get damaged during soldering:

![orderedCircuitBoard](doc/broken_pcb.jpg) 