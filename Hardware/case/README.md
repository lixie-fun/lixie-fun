**Content:**  

[[_TOC_]]

# Content of this directory
This directory includes STL files for the individual segments of the lixie clock and a casing for the microcontroller with the DCF antenna and the RTC.
Each display module consists of 3 parts: a **bottom** and a **top** part for the cover of the LEDs and a **cap** for holding the acrylic panes together at the top with the right distance.
The case for the controller is currently a work in progress and can be found in a different branch: [77-controller-gehause-design](https://gitlab.mi.hdm-stuttgart.de/embedded/ws19-20/lixie-fun/-/tree/77-controller-gehause-design/3D-Printing/10PanelSegment))
It consists of 2 parts: **controltop** and **control-bottom**. It can be connected to the other segments the same way they are connected to each other and has a little tower to keep the antenna at a safe distance to the electronics to avoid interference.

# Printing
The STL files still have to be sliced for the 3D-printer you're using.
Since these parts don't have to withstand a whole lot of pressure, the infill doesn't have to be very dense.
We used black PLA filament, since the base also glowed quite brightly when using white filament.
