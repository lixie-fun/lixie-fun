#ifndef DISPLAY_H
#define DISPLAY_H

#include <inttypes.h>
#include "lib/adc.h"
#include "config.h"

typedef struct
{
    uint8_t digit;
    uint32_t rgb;
}  display_segment;

uint8_t split_digit(uint8_t input, uint8_t digit_number);
void update_display(const display_segment arguments[]);

// Macro for reading the brightness control input and scaling it to the 5-bit value (0-31) needed for the LEDs
#define BRIGHTNESS_INPUT (uint8_t)(adc_read(PIN_BRIGHTNESS)*(31.0/1023.0))


#endif // DISPLAY_H