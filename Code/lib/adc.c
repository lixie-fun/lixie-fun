#include "adc.h"
#include "../config.h"
#include <avr/io.h>

/**
 * @brief Initializes the analog-to-digital converter and its associated IO ports (DDR_ADC and PIN_BRIGHTNESS)
 * 
 */
void adc_init()
{
    // Set Pin of the brightnes controller as input
    DDR_ADC &= ~(1 << PIN_BRIGHTNESS);
    // Set the reference voltage to AVcc
    ADMUX |= (1 << REFS0);
    // Set prescaler to 128 and enable ADC (ADEN)
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0) | (1 << ADEN);
}

/**
 * @brief Read a value from the ADC. 
 * 
 * @param ADCchannel the analog input pin from which to read
 * @return uint16_t A value ranging from 0 to 1023
 */
uint16_t adc_read(uint8_t ADCchannel)
{
    // Set ADC channel (last four bits in ADMUX)
    ADMUX = (ADMUX & 0xF0) | (ADCchannel & 0x0F);
    // Start the conversion.
    ADCSRA |= (1 << ADSC);
    // When the conversion finishes, the ADSC bit becomes zero again. Wait until that happens.
    while( ADCSRA & (1<<ADSC) );
    return ADC;
}

