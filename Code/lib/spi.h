#ifndef SPI_H
#define SPI_H

#include <inttypes.h>
void spi_master_init(void);
void spi_transmit(uint8_t cData);

#endif // SPI_H