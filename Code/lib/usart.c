#include <avr/io.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include "usart.h"
#include "dcf77.h"
#include "../config.h"

#define MYUBRR (FOSC/16/BAUD-1)
void USART_init(){
    #ifdef USE_USART

	UBRR0H = (uint8_t)(MYUBRR>>8);
	UBRR0L = (uint8_t)MYUBRR;
	/* Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set frame format: 8data, 2stop bit */
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
    
    #endif // USE_USART
}

void USART_transmit_char( uint8_t data ) {
    #ifdef USE_USART

	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) )
	;
	/* Put data into buffer, sends the data */
	UDR0 = data;
    
    #endif // USE_USART
}
void USART_print (const char *s) {
    #ifdef USE_USART

    while (*s)
    {
        USART_transmit_char(*s);
        s++;
    }
    
    #endif // USE_USART
}

void USART_printf (const char *s, ...) {
    #ifdef USE_USART

    char print_buffer[256];
    va_list args;
    va_start (args, s);
    vsprintf (print_buffer,s, args);
    va_end (args);

    USART_print(print_buffer);
    
    #endif // USE_USART
}

void USART_print_binary(const uint8_t bin) {
    #ifdef USE_USART

    char register_printout[8];
    itoa(bin, register_printout, 2);
    USART_printf("%s", register_printout);
    
    #endif // USE_USART
}

void USART_println_dcf_array() {
    #ifdef USE_USART

    for (size_t i = 0; i < array_count; i++)
    {
        USART_printf("%d", dcf_array[i]);
    }
    USART_printf("\n");
    
    #endif // USE_USART
}