#include "spi.h"
#include "../config.h"
#include <avr/io.h>


void spi_master_init(void) {
    /* Set MOSI, SCK and SS output */
    DDR_SPI |= (1<<DD_MOSI)|(1<<DD_SCK)|(1<<DD_SS);
    /* Enable SPI, Master mode, clock rate fck/16 */
    SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0)|(1<<SPR1);
}

void spi_transmit(uint8_t cData){
    SPDR = cData;
    while(!(SPSR & (1<<SPIF)))
    ;

}