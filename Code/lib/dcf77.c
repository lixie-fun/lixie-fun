#include "dcf77.h"
#include "usart.h"
#include "../config.h"
#include <string.h>
#include "../datetime.h"

// Macro to read the DCF pin
#define DCF_PIN_HIGH (PIN_DCF & (1<<DD_DCF))

// Macro for reading the timer in units of 10ms
#define TIMER_10MS ((TCNT1/156))

// struct to keep the datetime computed from the DCF77 signal
datetime _dcf_time;

/**
 * @brief Initializes the DCF antenna IO port, timer/counter 1 for counting signal times and enables external interrupts
 * 
 */
void dcf77_init (void) {
	must_compute_time = 0;
	signal_high_duration = 0;
	signal_low_duration = 0;
	dcf_signal_currently_high = 0;
	array_count = 0;
	timechange_announcement_counter = 0;

    // Set pin to input
    DDR_DCF &= ~(1 << DD_DCF);

	// # Timer Interrupt
	// Normal Timer Mode
	TCCR1A = 0;

	// Set Prescaler 1024
	TCCR1B |= (1<<CS10) | (1<<CS12);

	// No timer Interrupts
	TIMSK1 = 0;

	// Init counter
	TCNT1 = 0;

	// # External Interrupt
	// on ANY pin change
	EICRA = (1<<ISC00);

	// enable interrupt int0
	EIMSK |= (1<<INT0);
	sei();
}

/**
 * @brief clears dcf_array, resets the array counter and adds the leading 0 for minute-begin
 * We technically don't need to clear the array first, but is makes debug prints more readable if there is no leftover content from the last iteration
 */
void dcf77_reset_dcf_array(){
	memset((void*)dcf_array, 0, 60);
	array_count = 0;
	dcf_array[array_count++] = 0;
}

/**
 * @brief Computes the time from the data collected in dcf_array and updates _dcf_time
 * @return uint8_t 0 for success, 1 for failure
 */
uint8_t dcf77_compute_time(){

	// Flag gets set when we detect data corruption
	uint8_t data_corrupt = 0;

	// Parity counts the number of HIGHs we get and is later checked against the parity bit from the received data
	uint8_t parity = 0;

	// Reset hours and minutes for calculation
	_dcf_time.hour = 0;
	_dcf_time.minute = 0;
	_dcf_time.day = 0;
	_dcf_time.month = 0;
	_dcf_time.year = 0;
	_dcf_time.weekday = 0;

	
	// UTC Offset (the actual transmitted hour always matches Central European local time)
	if (dcf_array [16] == 1) {  // Announcement (for 1 hour before the switch)
		timechange_announcement_counter += 1;
	}
	if (dcf_array [17] == 0 && dcf_array [18] == 1 ) {	// Normal Time (UTC+1)
		_dcf_time.utc_offset = 1;
	}
	if (dcf_array [17] == 1 && dcf_array [18] == 0 ) {	// Summer Time (UTC+2)
		_dcf_time.utc_offset = 2;
	}

	// Compute hours
	if (dcf_array [29] == 1) {  //stunde 1
		_dcf_time.hour = 1;
		parity++;
	}
	if (dcf_array [30] == 1) {  //stunde 2
		_dcf_time.hour += 2;
		parity++;
	}
	if (dcf_array [31] == 1) {  //stunde 4 
		_dcf_time.hour += 4;
		parity++;
	}
	if (dcf_array [32] == 1) {  //stunde 8
		_dcf_time.hour += 8;
		parity++;
	}
	if (dcf_array [33] == 1) {  //stunde 10
		_dcf_time.hour += 10;
		parity++;
	}
	if (dcf_array [34] == 1) {  //stunde 20
		_dcf_time.hour += 20;
		parity++;
	}
	// If the parity bit is set, we expect to have gotten an UNEVEN number of HIGHs 
	if(dcf_array [35] == 1) {
		if(parity%2 == 0){
			USART_printf("Bad parity (hours)\n");
			data_corrupt = 1;
		}
	}
	// If the parity bit is NOT set, we expect to have gotten an EVEN number of HIGHs 
	if(dcf_array [35] == 0) {
		if(parity%2 != 0){
			USART_printf("Bad parity (hours)\n");
			data_corrupt = 1;
		}
	}
	parity = 0;

	// Compute minutes
	if (dcf_array [21] == 1) {  //minute 1
		_dcf_time.minute = 1;
		parity++;
	}
	if (dcf_array [22] == 1) {  //minute 2
		_dcf_time.minute += 2;
		parity++;
	}
	if (dcf_array [23] == 1) {  //minute 4
		_dcf_time.minute += 4;
		parity++;
	}
	if (dcf_array [24] == 1) {  //minute 8
		_dcf_time.minute += 8;
		parity++;
	}
	if (dcf_array [25] == 1) {  //minute 10
		_dcf_time.minute += 10;
		parity++;
	}
	if (dcf_array [26] == 1) {  //minute 20
		_dcf_time.minute += 20;
		parity++;
	}
	if (dcf_array [27] == 1) {  //minute 40
		_dcf_time.minute += 40;
		parity++;
	}
	// If the parity bit is set, we expect to have gotten an UNEVEN number of HIGHs 
	if(dcf_array [28] == 1) {
		if(parity%2 == 0){
			USART_printf("Bad parity (minutes)\n");
			data_corrupt = 1;
		}
	}
	// If the parity bit is NOT set, we expect to have gotten an EVEN number of HIGHs 
	if(dcf_array [28] == 0) {
		if(parity%2 != 0){
			USART_printf("Bad parity (minutes)\n");
			data_corrupt = 1;
		}
	}
	parity = 0;

	//Compute calendar day
	if (dcf_array [36] == 1) {  //calendar day 1
		_dcf_time.day += 1;
		parity++;
	}
	if (dcf_array [37] == 1) {  //calendar day  2
		_dcf_time.day += 2;
		parity++;
	}
	if (dcf_array [38] == 1) {  //calendar day  4
		_dcf_time.day += 4;
		parity++;
	}
	if (dcf_array [39] == 1) {  //calendar day  8
		_dcf_time.day += 8;
		parity++;
	}
	if (dcf_array [40] == 1) {  //calendar day  10
		_dcf_time.day += 10;
		parity++;
	}
	if (dcf_array [41] == 1) {  //calendar day  20
		_dcf_time.day += 20;
		parity++;
	}

	//compute weekday
	if (dcf_array [42] == 1) {  //weekday 1
		_dcf_time.weekday += 1;
		parity++;
	}
	if (dcf_array [43] == 1) {  //weekday 2
		_dcf_time.weekday += 2;
		parity++;
	}
	if (dcf_array [44] == 1) {  //weekday 4
		_dcf_time.weekday += 4;
		parity++;
	}

	//Compute Month
	if (dcf_array [45] == 1) {  //month 1
		_dcf_time.month += 1;
		parity++;
	}
	if (dcf_array [46] == 1) {  //month 2
		_dcf_time.month += 2;
		parity++;
	}
	if (dcf_array [47] == 1) {  //month 4
		_dcf_time.month += 4;
		parity++;
	}
	if (dcf_array [48] == 1) {  //month 8
		_dcf_time.month += 8;
		parity++;
	}
	if (dcf_array [49] == 1) {  //month 10
		_dcf_time.month += 10;
		parity++;
	}

	//Compute year
	if (dcf_array [50] == 1) {  //year 1
		_dcf_time.year += 1;
		parity++;
	}
	if (dcf_array [51] == 1) {  //year 2
		_dcf_time.year += 2;
		parity++;
	}
	if (dcf_array [52] == 1) {  //year 4
		_dcf_time.year += 4;
		parity++;
	}
	if (dcf_array [53] == 1) {  //year 8
		_dcf_time.year += 8;
		parity++;
	}
	if (dcf_array [54] == 1) {  //year 10
		_dcf_time.year += 10;
		parity++;
	}
	if (dcf_array [55] == 1) {  //year 20
		_dcf_time.year += 20;
		parity++;
	}
	if (dcf_array [56] == 1) {  //year 40
		_dcf_time.year += 40;
		parity++;
	}
	if (dcf_array [57] == 1) {  //year 80
		_dcf_time.year += 80;
		parity++;
	}
	// If the parity bit is set, we expect to have gotten an UNEVEN number of HIGHs 
	if(dcf_array [58] == 1) {
		if(parity%2 == 0){
			USART_printf("Bad parity (date)\n");
			data_corrupt = 1;
		}
	}
	// If the parity bit is NOT set, we expect to have gotten an EVEN number of HIGHs 
	if(dcf_array [58] == 0) {
		if(parity%2 != 0){
			USART_printf("Bad parity (date)\n");
			data_corrupt = 1;
		}
	}
	parity = 0;

	// check for logical time values
	// TODO
	if(_dcf_time.hour < 0 || _dcf_time.hour > 23 || 
		_dcf_time.minute < 0 || _dcf_time.minute > 59 ||
		_dcf_time.day < 0 || _dcf_time.day > 31 ||
		_dcf_time.month < 0 || _dcf_time.month > 12 ||
		_dcf_time.year < 0) 
	{
		USART_printf("Time values out of bounds\n");
		data_corrupt = 1;
	}
	
	// Handle the case if we get corrupt data
	if (data_corrupt) {
		USART_printf("Got corrupt data this cycle.\n");
		return 1;
	} 

	USART_printf("DCF TIME: - %d.%d.%d - %d:%d - UTC-Offset: %d\n", _dcf_time.day, _dcf_time.month, _dcf_time.year, _dcf_time.hour, _dcf_time.minute, _dcf_time.utc_offset);
	
	return 0;
}

/**
 * @brief Interrupt handler for changes on the DCF signal input pin.
 * 
 */
ISR (INT0_vect) {
	if (DCF_PIN_HIGH) {
		// HIGH beginning
		signal_low_duration = TIMER_10MS;
		TCNT1 = 0;
		signal_low_duration_print = signal_low_duration;
		
		// If the previous LOW was longer than 1500 ms, we most likely just passed the syncronisation gap, aka a new minute has begun
		if (signal_low_duration > 150) {
			must_compute_time = 1;
		}
		
	} else {
		// HIGH ending
		signal_high_duration = TIMER_10MS;
		TCNT1 = 0;

		// tolerance +20ms, -50ms
		if (signal_high_duration >= 5 && signal_high_duration <= 12) {
			signal_high_duration_print = signal_high_duration;
			dcf_array[array_count++] = 0;
		} else if (signal_high_duration >= 15 && signal_high_duration <= 22) {
			signal_high_duration_print = signal_high_duration;
			dcf_array[array_count++] = 1;
		} else {
			
		}
	}
}