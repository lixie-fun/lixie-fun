#ifndef USART_H
#define USART_H

void USART_init();
void USART_printf (const char *s, ...);
void USART_print_binary(uint8_t bin);
void USART_println_dcf_array();

#endif // USART_H