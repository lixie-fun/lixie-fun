#ifndef DCF77_H
#define DCF77_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <inttypes.h>
#include "../datetime.h"

extern datetime _dcf_time;
volatile uint8_t must_compute_time;
volatile uint16_t signal_high_duration;
volatile int16_t signal_high_duration_print;
volatile int16_t signal_low_duration_print;
volatile uint16_t signal_low_duration;
volatile uint8_t dcf_signal_currently_high;
volatile uint8_t array_count;
volatile char dcf_array [60];
volatile uint8_t timechange_announcement_counter;

void dcf77_init (void);
void dcf77_reset_dcf_array(void);
uint8_t dcf77_compute_time (void);

#endif // DCF77_H



