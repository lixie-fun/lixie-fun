#ifndef DATETIME_H
#define DATETIME_H
#include <inttypes.h>

typedef struct 
{
    uint8_t second;
    uint8_t minute;
    uint8_t hour;
    uint8_t day;
    uint8_t weekday;
    uint8_t month;
    uint8_t year;
    uint8_t utc_offset;
} datetime;

void tick_seconds(datetime* dt);

#endif // DATETIME_H