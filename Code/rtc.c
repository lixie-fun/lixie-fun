#include <util/delay.h>
#include "lib/i2c_master.h"
#include "rtc.h"
#include "lib/usart.h"
#include "config.h"

#define RTC_READ_ADDR ((RTC_ADDR<<1)| I2C_READ)
#define RTC_WRITE_ADDR ((RTC_ADDR<<1)| I2C_WRITE)
#define RTC_MEM_READ_ADDR ((RTC_MEM_ADDR<<1)| I2C_READ)
#define RTC_MEM_WRITE_ADDR ((RTC_MEM_ADDR<<1)| I2C_WRITE)

datetime _rtc_time;
temperature_t _rtc_temp;
uint8_t _rtc_mem_buffer = 0;
/**
 * @brief converts a plain decimal to a binary coded decimal
 * 
 * @param d decimal
 * @return uint8_t binary coded decimal
 */
uint8_t dec2bcd(uint8_t d)
{
  return ((d/10 * 16) + (d % 10));
}

/**
 * @brief converts a binary coded decimal into a plain decimal
 * 
 * @param b binary coded decimal
 * @return uint8_t plain decimal
 */
uint8_t bcd2dec(uint8_t b)
{
  return ((b/16 * 10) + (b % 16));
}

/**
 * @brief reads time data from the RTC module
 * 
 * @return uint8_t 1 for failure, 0 for success
 */
uint8_t rtc_read_time(){
    if(i2c_start(RTC_WRITE_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }
    
    if(i2c_write(RTC_TIME_REGISTER_OFFSET)){
        USART_printf("Sending Register pointer failed\n");
        return 1; 
    }

    if(i2c_start(RTC_READ_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }
    _rtc_time.second = bcd2dec(i2c_read_ack());
    _rtc_time.minute = bcd2dec(i2c_read_ack());
    _rtc_time.hour = bcd2dec(i2c_read_ack());
    _rtc_time.weekday = bcd2dec(i2c_read_ack()); // position 3 is weekday - not used, but read it anyway to increment the read pointer
    _rtc_time.day = bcd2dec(i2c_read_ack());
    _rtc_time.month = bcd2dec(i2c_read_ack());
    _rtc_time.year = bcd2dec(i2c_read_nack());
    i2c_stop();

    return 0;

}

/**
 * @brief Writes time to the RTC module
 * 
 * @param hr hour
 * @param min minute
 * @param sec second
 * @return uint8_t 1 for failure, 0 for success
 */
uint8_t rtc_write_time(uint8_t hr, uint8_t min, uint8_t sec, uint8_t d, uint8_t m, uint8_t y, uint8_t wd){

    if(i2c_start(RTC_WRITE_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }

    if(i2c_write(RTC_TIME_REGISTER_OFFSET)){
        USART_printf("Sending Register pointer failed\n");        
        return 1; 
    }

    if(i2c_write(dec2bcd(sec))){
        USART_printf("Sending seconds failed\n");
        return 1; 
    }

    if(i2c_write(dec2bcd(min))){
        USART_printf("Sending minutes failed\n");
        return 1; 
    }

    if(i2c_write(dec2bcd(hr))){
        USART_printf("Sending hours failed\n");
        return 1; 
    }
    
    if(i2c_write(dec2bcd(wd))){
        USART_printf("Sending weekday failed\n");
        return 1; 
    }

    if(i2c_write(dec2bcd(d))){
        USART_printf("Sending day failed\n");
        return 1; 
    }

    if(i2c_write(dec2bcd(m))){
        USART_printf("Sending month failed\n");
        return 1; 
    }

    if(i2c_write(dec2bcd(y))){
        USART_printf("Sending year failed\n");
        return 1; 
    }

    i2c_stop();
    
    return 0;
}


/**
 * @brief reads temperature data from the RTC module
 * 
 * @return uint8_t 1 for failure, 0 for success
 */
uint8_t rtc_read_temperature(){
    if(i2c_start(RTC_WRITE_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }
    
    if(i2c_write(RTC_TEMP_REGISTER_OFFSET)){
        USART_printf("Sending Register pointer failed\n");
        return 1; 
    }

    if(i2c_start(RTC_READ_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }
    _rtc_temp.integer_part = i2c_read_ack();
    _rtc_temp.fractional_part = (i2c_read_nack() >> 6) * 25;
    i2c_stop();

    return 0;

}



/**
 * @brief reads data from the RTC module's eeprom
 * 
 * @param uint16_t address the memory address to read
 * @param uint8_t* pointer to write the read data to
 * @return uint8_t 1 for failure, 0 for success
 */
uint8_t rtc_mem_read(uint16_t address, uint8_t* output){
    if(i2c_start(RTC_MEM_WRITE_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }

    // two address words since the address range goes up to 4096, which needs 12 bits. (the first four bits of the first word are ignored)
    if(i2c_write(address >> 8)){ // first half: shift right by 1 byte 
        USART_printf("Sending Register pointer 1 failed\n");        
        return 1; 
    }
    if(i2c_write(address & 0xff)){ // second half: bitAND with a full 1-byte to get rid of the first half
        USART_printf("Sending Register pointer 2 failed\n");        
        return 1; 
    }

    if(i2c_start(RTC_MEM_READ_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }

    _rtc_mem_buffer = i2c_read_nack();
    i2c_stop();

    return 0;
}

/**
 * @brief Writes data to the RTC module's eeprom 
 * 
 * @param data the data
 * @return uint8_t 1 for failure, 0 for success
 */
uint8_t rtc_mem_write(uint16_t address, uint8_t data){

    if(i2c_start(RTC_MEM_WRITE_ADDR)){ 
        USART_printf("Error sending Start condition\n");
        return 1; 
    }

    // two address words since the address range goes up to 4096, which needs 12 bits. (the first four bits of the first word are ignored)
    if(i2c_write(address >> 8)){ // first half: shift right by 1 byte 
        USART_printf("Sending Register pointer 1 failed\n");        
        return 1; 
    }
    if(i2c_write(address & 0xff)){ // second half: bitAND with a full 1-byte to get rid of the first half
        USART_printf("Sending Register pointer 2 failed\n");        
        return 1; 
    }

    if(i2c_write(data)){
        USART_printf("Sending data failed\n");
        return 1; 
    }

    i2c_stop();

    // The writing takes a maximum of 5ms during which the eeprom won't respond. Sleep to ensure the write finishes before accessing the device again.
    _delay_ms(5);
    
    return 0;
}