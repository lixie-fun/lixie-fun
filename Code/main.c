#include "display.h"
#include "lib/adc.h"
#include "lib/dcf77.h"
#include "lib/usart.h"
#include "rtc.h"
#include <avr/io.h>
#include "lib/spi.h"
#include "config.h"
#include "datetime.h"

datetime _main_time;

int main()
{

    // initialize SPI master and associated IO ports
    spi_master_init();
    
    // initialize the ADC and associated IO ports
    adc_init();
    
    // initialize USART
    USART_init();
    
    i2c_init();

    // Start the display
    display_segment display[NUM_SEGMENTS];
    
    display[0] = (display_segment){1, COLOR_INIT};
    display[1] = (display_segment){1, COLOR_INIT};
    display[2] = (display_segment){1, COLOR_INIT};
    display[3] = (display_segment){1, COLOR_INIT};
    display[4] = (display_segment){1, COLOR_INIT};
    display[5] = (display_segment){1, COLOR_INIT};
    
    update_display(display);

    // initialize DCF and associated IO ports
    dcf77_init();

    // initialize internal time 
    _main_time.hour = 0;
    _main_time.minute = 0;
    _main_time.second = 0;


    // Variable to check when dcf_array has been modified
    uint8_t lastArrayCount = -1;
    uint8_t lastSecond = -1;
    enum mode { SIGNAL_FOUND = 1, SIGNAL_SEARCHING = 0} display_mode;
    // Always read/write the valid signal counter to memory to protect it from reset when board gets connected to a pc.
    rtc_mem_read(RTC_MEM_REGISTER_OFFSET_VALIDSIGNAL, &_rtc_mem_buffer);
    USART_printf("Had %d valid signals since the last bad one\n", _rtc_mem_buffer);

    uint8_t utc_offset_mismatch_counter = -1;

    // RTC and DCF Mode
    #define SWITCH_TO_RTC_MODE 0
    #define ALREADY_IN_RTC_MODE 1
    rtc_mem_read(RTC_MEM_REGISTER_OFFSET_OPMODE, &_rtc_mem_buffer);
    if(_rtc_mem_buffer != ALREADY_IN_RTC_MODE){
        display_mode = SIGNAL_SEARCHING;
    } else {
        display_mode = SIGNAL_FOUND;
    }
    USART_printf("Starting in mode: %d\n", display_mode);

    rtc_mem_read(RTC_MEM_REGISTER_OFFSET_UTCOFFSET, &_rtc_mem_buffer);
    USART_printf("UTC Offset is %d\n", _rtc_mem_buffer);

    while (1)
    {
        // In every case, read the data from the RTC chip to get a second clock
        rtc_read_time();
        // rtc_read_temperature();

        // This runs everytime we didn't receive a signal for about 4 seconds (aka the timer overflows)
        // at this point, we definitely have missed some bits, so we need to start over.
        if((TIFR1 & (1<<TOV1)) == 1){
            USART_printf("No Signal!\n");
            // Reset the overflow flag
            TIFR1 |= (1<<TOV1);

            dcf77_reset_dcf_array();
        }     

        // This runs everytime a DCF bit is registered (with ideal signal, every second)
        if(array_count != lastArrayCount){
            uint16_t adc_raw = adc_read(PIN_BRIGHTNESS);
            USART_printf("DCF - bit %d: %d - LOW/HIGH (ms): %d/%d - Raw ADC: %d - Brightness: %d\n", array_count-1, dcf_array[array_count-1], signal_low_duration_print*10, signal_high_duration_print*10, adc_raw, BRIGHTNESS_INPUT);
            signal_high_duration_print = -1;
            signal_low_duration_print = -1;

            if(must_compute_time==1){
                must_compute_time = 0;
                USART_printf("in must_compute_time with array_count %d: ", array_count);
                USART_println_dcf_array();
                if(array_count >= 59) {
                    datetime old_time = _dcf_time;
                    if(dcf77_compute_time() == 0){ // !!! updates _dcf_time
                        rtc_mem_read(RTC_MEM_REGISTER_OFFSET_VALIDSIGNAL, &_rtc_mem_buffer);
                        USART_printf("Had %d valid signals since the last bad one\n", _rtc_mem_buffer);

                        rtc_mem_read(RTC_MEM_REGISTER_OFFSET_UTCOFFSET, &_rtc_mem_buffer);
                        _rtc_time.utc_offset = _rtc_mem_buffer;
                        if(_rtc_time.utc_offset != _dcf_time.utc_offset){
                            utc_offset_mismatch_counter += 1;
                        } else {
                            utc_offset_mismatch_counter = 0;
                        }

                        // (re)write time to RTC if ...
                        uint8_t rewrite_rtc = 0;
                        // - we were still in signal searching mode and initially get a valid signal 
                        rtc_mem_read(RTC_MEM_REGISTER_OFFSET_OPMODE, &_rtc_mem_buffer);
                        if(_rtc_mem_buffer != ALREADY_IN_RTC_MODE ) {
                            rewrite_rtc = 1;
                            USART_printf("Got valid data.");
                            display_mode = SIGNAL_FOUND;
                            rtc_mem_write(RTC_MEM_REGISTER_OFFSET_OPMODE, ALREADY_IN_RTC_MODE);
                        }
                        // - utc offset changes and the timechange was announced
                        if(old_time.utc_offset != _dcf_time.utc_offset && (timechange_announcement_counter >= RTC_UPDATE_UTC_OFFSET_ANNOUNCE)) {
                            rewrite_rtc = 1;
                            USART_printf("UTC offset change detected. ");
                            timechange_announcement_counter = 0;
                        }
                        // - utc offset has changed since last time we were on, and the mismatch has been consistent for 3 minutes
                        if(utc_offset_mismatch_counter >= RTC_UPDATE_UTC_MISMATCH ) {
                            USART_printf("UTC offset changed during offline time. ");
                            rewrite_rtc = 1;
                        }
                        // - we had x valid signals in a row (to prevent RTC module from becoming unaccurate)
                        rtc_mem_read(RTC_MEM_REGISTER_OFFSET_VALIDSIGNAL, &_rtc_mem_buffer);
                        if(_rtc_mem_buffer % RTC_UPDATE_AFTER_VALID_MINUTES == 0) {
                            rewrite_rtc = 1;
                            USART_printf("Got valid data for %d minutes.", _rtc_mem_buffer);
                        }

                        // Update the RTC module with DCF signal data
                        if(rewrite_rtc == 1){
                            rewrite_rtc = 0;
                            USART_printf(" Writing to RTC.\n");
                            rtc_write_time(_dcf_time.hour, _dcf_time.minute, 0, _dcf_time.day, _dcf_time.month, _dcf_time.year, _dcf_time.weekday);
                            rtc_mem_write(RTC_MEM_REGISTER_OFFSET_UTCOFFSET, _dcf_time.utc_offset);
                        }

                        // increment the DCF valid signal counter
                        rtc_mem_read(RTC_MEM_REGISTER_OFFSET_VALIDSIGNAL, &_rtc_mem_buffer);
                        rtc_mem_write(RTC_MEM_REGISTER_OFFSET_VALIDSIGNAL, _rtc_mem_buffer+1);

                    } else {
                        // reset the DCF valid signal counter
                        rtc_mem_write(RTC_MEM_REGISTER_OFFSET_UTCOFFSET, 0);
                    }
                } else {
                    USART_printf(" insufficient data.\n");
                    // reset the DCF valid signal counter
                    rtc_mem_write(RTC_MEM_REGISTER_OFFSET_UTCOFFSET, 0);
                }
                dcf77_reset_dcf_array();
            }
            // if the array runs full without having passed a sync gap (aka when must_compute_time gets set), something is wrong, so reset
            if(array_count > 59){
                dcf77_reset_dcf_array();
            }
            lastArrayCount = array_count;
        }

        switch (display_mode)
        {
        case SIGNAL_SEARCHING:
            // update the display every second
            if(_rtc_time.second != lastSecond){
                tick_seconds(&_main_time);
                USART_printf("DCF %d:%d:%d\n", _main_time.hour, _main_time.minute, _main_time.second);

                display[0] = (display_segment){split_digit(_main_time.hour, 10),   COLOR_DCF};
                display[1] = (display_segment){split_digit(_main_time.hour, 1),    COLOR_DCF};
                display[2] = (display_segment){split_digit(_main_time.minute, 10), COLOR_DCF};
                display[3] = (display_segment){split_digit(_main_time.minute, 1),  COLOR_DCF};
                display[4] = (display_segment){split_digit(_main_time.second, 10), COLOR_DCF};
                display[5] = (display_segment){split_digit(_main_time.second, 1),  COLOR_DCF};
                lastSecond = _rtc_time.second;
            }
            
            update_display(display);
        break;

        default:
        case SIGNAL_FOUND:

            // This runs every second
            if(_rtc_time.second != lastSecond){
                
                uint16_t adc_raw = adc_read(PIN_BRIGHTNESS);

                // Date
                if ( (_rtc_time.second > 10 && _rtc_time.second < 14) || (_rtc_time.second > 40 && _rtc_time.second < 44) ) {
                    display[0] = (display_segment){split_digit(_rtc_time.day, 10),   COLOR_RTC_1};
                    display[1] = (display_segment){split_digit(_rtc_time.day, 1),    COLOR_RTC_1};
                    display[2] = (display_segment){split_digit(_rtc_time.month, 10), COLOR_RTC_2};
                    display[3] = (display_segment){split_digit(_rtc_time.month, 1),  COLOR_RTC_2};
                    display[4] = (display_segment){split_digit(_rtc_time.year, 10),  COLOR_RTC_3};
                    display[5] = (display_segment){split_digit(_rtc_time.year, 1),   COLOR_RTC_3};
                
                // Temperature
                // } else if ( (_rtc_time.second > 20 && _rtc_time.second < 24) || (_rtc_time.second > 50 && _rtc_time.second < 54) ) {
                //     uint16_t adc_raw = adc_read(PIN_BRIGHTNESS);
                //     USART_printf("RTC - %d.%d.%d - %d:%d:%d - *%d.%d °C - Raw ADC: %d - Brightness: %d\n", _rtc_time.day, _rtc_time.month, _rtc_time.year, _rtc_time.hour, _rtc_time.minute, _rtc_time.second, _rtc_temp.integer_part, _rtc_temp.fractional_part, adc_raw, BRIGHTNESS_INPUT);
                //     display[0] = (display_segment){0, COLOR_OFF};
                //     display[1] = (display_segment){0, COLOR_OFF};
                //     display[2] = (display_segment){split_digit(_rtc_temp.integer_part, 10), COLOR_RTC_2};
                //     display[3] = (display_segment){split_digit(_rtc_temp.integer_part, 1),  COLOR_RTC_2};
                //     display[4] = (display_segment){split_digit(_rtc_temp.fractional_part, 10), COLOR_RTC_3};
                //     display[5] = (display_segment){split_digit(_rtc_temp.fractional_part, 1),  COLOR_RTC_3};

                // Default: Time
                } else {
                    display[0] = (display_segment){split_digit(_rtc_time.hour, 10),   COLOR_RTC_1};
                    display[1] = (display_segment){split_digit(_rtc_time.hour, 1),    COLOR_RTC_1};
                    display[2] = (display_segment){split_digit(_rtc_time.minute, 10), COLOR_RTC_2};
                    display[3] = (display_segment){split_digit(_rtc_time.minute, 1),  COLOR_RTC_2};
                    display[4] = (display_segment){split_digit(_rtc_time.second, 10), COLOR_RTC_3};
                    display[5] = (display_segment){split_digit(_rtc_time.second, 1),  COLOR_RTC_3};
                }
                USART_printf("RTC - *%d.%d.%d - %d:%d:%d - UTC-Offset: %d - Raw ADC: %d - Brightness: %d\n", _rtc_time.day, _rtc_time.month, _rtc_time.year, _rtc_time.hour, _rtc_time.minute, _rtc_time.second, _rtc_time.utc_offset, adc_raw, BRIGHTNESS_INPUT);
                
                lastSecond = _rtc_time.second;
            }
            update_display(display);

        break;
        }


    }
}
