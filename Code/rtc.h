#ifndef RTC_H
#define RTC_H

#include <inttypes.h>
#include "datetime.h"
#include "temperature.h"
#include "lib/i2c_master.h"

extern datetime _rtc_time;
extern temperature_t _rtc_temp;
extern uint8_t _rtc_mem_buffer;

uint8_t rtc_read_temperature();
uint8_t rtc_read_time();
uint8_t rtc_write_time(uint8_t hr, uint8_t min, uint8_t sec, uint8_t d, uint8_t m, uint8_t y, uint8_t wd); 

uint8_t rtc_mem_read(uint16_t address, uint8_t* output);
uint8_t rtc_mem_write(uint16_t address, uint8_t data);

#endif // RTC_H