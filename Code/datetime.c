#include "datetime.h"

/**
 * @brief Increases dt->hour by one, handles hour overflow (hours roll over to 0 after 23)
 * @param  dt: the datetime structure to tick up
 */
void tick_hours(datetime* dt){
	dt->hour = (dt->hour+1)%24;
}

/**
 * @brief Increases dt->minute by one, handles minute overflow (minutes roll over to 0 after 59)
 * @param  dt: the datetime structure to tick up
 */
void tick_minutes(datetime* dt){
	if(++dt->minute == 60) {
		tick_hours(dt);
        dt->minute = 0;
	}
}
/**
 * @brief Increases dt->second by one, handles second overflow (seconds roll over to 0 after 59)
 * @param  dt: the datetime structure to tick up
 */
void tick_seconds(datetime* dt){
    if(++dt->second == 60){
        tick_minutes(dt);
        dt->second = 0;
    }
}