#include "display.h"
#include "lib/adc.h"
#include "lib/spi.h"
#include "config.h"

// Macro for the total number of LEDs
#define NUM_LEDS NUM_SEGMENTS*20

// Macro for reading the brightness control input and scaling it to the 5-bit value (0-31) needed for the LEDs
#define BRIGHTNESS_INPUT (uint8_t)(adc_read(PIN_BRIGHTNESS)*(31.0/1023.0))

/**
 * @brief gets a specific digit out of a number. For example, split_digit(25, 1) returns 5. split_digit(25, 10) returns 2.
 * 
 * @param input the number
 * @param digit_number which digit to split off. 1 for the 1-digit, 10 for the 10-digit
 * @return uint8_t the single digit
 */
uint8_t split_digit(uint8_t input, uint8_t digit_number){
    switch (digit_number)
    {
    case 1:
    default:
        return input%10;
    case 10:
        return (input/10)%10;
    }
}

/**
 * @brief calculates the exact position of digit x on segment y.
 * 
 * @param segment the segment
 * @param number the digit
 * @return uint16_t the absolute position of the _first_ LED corresponding to the desired digit (each digit is lit by two LEDs)
 */
uint16_t get_LED_position(const uint8_t segment, const uint8_t number){
    return 20*segment + 2*number;
}

/**
 * @brief updates the display with the digits and colors specified in the argument
 * 
 * @param display a display_segment array with length NUM_SEGMENTS.
 */
void update_display(const display_segment display[]) {
    // initialize all leds with RGB 0, aka off.
    uint32_t leds_rgb[NUM_LEDS] = {0};

    // for the specified digits, set the corresponding LEDs to their RGB value
    for (uint16_t i = 0; i < NUM_SEGMENTS; i++)
    {
        uint16_t offset = get_LED_position(i, (display[i].digit));

        // set first LED
        leds_rgb[offset] = display[i].rgb;
        // set second LED 
        leds_rgb[offset+1] = display[i].rgb;

    }

    // 1. Start Frame
    // * The start frame consists of 32 0-bits.
    spi_transmit(0b00000000);
    spi_transmit(0b00000000);
    spi_transmit(0b00000000);
    spi_transmit(0b00000000);

    // 2. LED Frames
    for (uint16_t i = 0; i < NUM_LEDS; i++)
    {
        // Start bits (3*1) and brightness combined into one byte
        // brightness has only 5 bits, so it ranges from 0 to 31
        spi_transmit((0b111 << 5) | BRIGHTNESS_INPUT);
        // Actually send the RGB fields (order: B-G-R!)
        spi_transmit( (uint8_t)(leds_rgb[i]) );
        spi_transmit( (uint8_t)(leds_rgb[i]>>8) );
        spi_transmit( (uint8_t)(leds_rgb[i]>>16) );
    }

    // 3. End Frame
    // Omitting the end frame causes the physically last LED in the chain to react one update cycle delayed.
    // The end frame consists of 32 1-bits according to SK9822 spec. 
    // However, tests have shown that 64 bits are needed to avoid this, 32 is not enough.
    // Possibly, the number of required bits is related to the amount of LEDs in the chain. (That's how it is described in the APA102 spec.)
    spi_transmit(0b11111111);
    spi_transmit(0b11111111);
    spi_transmit(0b11111111);
    spi_transmit(0b11111111);

    spi_transmit(0b11111111);
    spi_transmit(0b11111111);
    spi_transmit(0b11111111);
    spi_transmit(0b11111111);

}