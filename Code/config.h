#ifndef CONFIG_H
#define CONFIG_H

// General
#define F_CPU 16000000UL

// USART
#define USE_USART // comment this line to disable USART
#define FOSC 16000000
#define BAUD 9600

// DCF (antenna)
#define DDR_DCF DDRD
#define PIN_DCF PIND
#define DD_DCF DD2

// SPI (led control)
#define DDR_SPI DDRB
#define DD_MOSI PORTB3
#define DD_SCK PORTB5
#define DD_SS PORTB2

// ADC (LED brightness control)
#define DDR_ADC DDRC
#define PIN_BRIGHTNESS PORTC0

// RTC module
#define RTC_ADDR 0x68
#define RTC_TIME_REGISTER_OFFSET 0x00
#define RTC_TEMP_REGISTER_OFFSET 0x11
#define RTC_MEM_ADDR 0x57
#define RTC_MEM_REGISTER_OFFSET_OPMODE 0x000
#define RTC_MEM_REGISTER_OFFSET_UTCOFFSET 0x001
#define RTC_MEM_REGISTER_OFFSET_VALIDSIGNAL 0x002

// Update RTC
#define RTC_UPDATE_AFTER_VALID_MINUTES 60 // Max: 255
#define RTC_UPDATE_UTC_OFFSET_ANNOUNCE 55
#define RTC_UPDATE_UTC_MISMATCH 2

// display
#define NUM_SEGMENTS (uint8_t)6

// Display Colors
#define COLOR_INIT 0xFF0000
#define COLOR_DCF 0xFF8000
#define COLOR_RTC_1 0x00c2d0
#define COLOR_RTC_2 0x66ff66
#define COLOR_RTC_3 0xffffff
#define COLOR_OFF 0x000000


#endif // CONFIG_H