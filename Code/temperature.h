#ifndef TEMPERATURE_H
#define TEMPERATURE_H
#include <inttypes.h>

typedef struct 
{
    uint8_t integer_part;
    uint8_t fractional_part;
} temperature_t;

#endif // TEMPERATURE_H