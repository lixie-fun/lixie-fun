We divide our code into _libraries_ and _application code_. This is a high-level overview of all code files and their purpose. A detailed description of every function can be found in each .c file.

**Content:**  

[[_TOC_]]


# main.c
The main program consists of two operation modes: 
1. Synchronizing with the *DCF* signal
2. Displaying the time from the RTC module

The program starts in DCF mode, where it collects the bits received from the antenna. Once a valid time signal has been received, the time is decoded and written to the RTC module. Now, the program switches into RTC mode and simply displays the time taken from the RTC module.

The high level program flow can be seen in the following diagram:  

![main loop](doc/main_loop_flow-States.jpg)


# config.h
The following variables can be configured:

* **General**
    * F_CPU  
      The Processor frequency

* **USART**
    * USE_USART  
      Enable or disable USART prints
    * FOSC  
      Oscillator frequency
    * BAUD  
      Baud rate

* **IO Ports**
    * DCF (antenna)
        * DDR_DCF  
          The DDR register of the port which has the pin connected to the antenna
        * PIN_DCF  
          The PIN register of the port which has the pin connected to the antenna
        * DD_DCF  
          The bit of the pin which is connected to the antenna
    * SPI (led control)
        * DDR_SPI  
          The DDR register of the port which has the pins connected to the LEDs
        * DD_MOSI  
          The bit of the pin which is used as MOSI
        * DD_SCK  
          The bit of the pin which is used as SCK
        * DD_SS  
          The bit of the pin which is used as SS
    * ADC (LED brightness control)
        * DDR_ADC  
          The DDR register of the port which has the pins connected to the brightness controller
        * PIN_BRIGHTNESS  
          The bit of the pin which is connected to the brightness controller
          
* **RTC module device address**
    * RTC_ADDR  
      The I2C address of the RTC module
    * RTC_TIME_REGISTER_OFFSET  
      The register address from which to read within the RTC module

* **Display Size**
    * NUM_SEGMENTS  
      The number of display segments

* **Display Colors**
    * COLOR_INIT  
      Color of the display during initialization
    * COLOR_DCF  
      Color of the display during DCF synchronization
    * COLOR_RTC_1  
      Color of the first two digits during RTC time display
    * COLOR_RTC_2  
      Color of the second two digits during RTC time display
    * COLOR_RTC_3  
      Color of the third two digits during RTC time display

# display
Code to update the display with an array of digit+color data.

# rtc
Code to read from and write to the **DS3231** RTC module.

# datetime
Definition of a `datetime` struct, and functions to advance time.  
The `datetime` struct contains the following information:  
* second
* minute
* hour
* day
* weekday (unused)
* month
* year

# lib/dcf77
Code to for the *DCF77* antenna to receive and decode time data.  
**dcf77.c** contains the interrupt service routine (ISR) which is executed on any change of the state of the IO pin connected to the DCF signal. The ISR measures the duration of `HIGH` and `LOW`s, registering `1` or `0` depending on the `HIGH` duration, and triggers the signal decoding at the start of a new minute:  

![isr](doc/main_loop_flow-ISR.jpg)

# lib/adc
Code to initialize and read the analog-to-digital converter. 

# lib/i2c_master
Code for *I2C*, (in Atmel context known as *2-wire* or *TWI*) interface. [I2C Master library](https://github.com/knightshrub/I2C-master-lib) *Copyright (C) 2019 Elia Ritterbusch*.  

# lib/spi
Code for the Serial Peripheral Interface (SPI).

# lib/usart
Code for the universal synchronous and asynchronous serial receiver and transmitter (USART).
