# **Note: This Project was moved to [gitlab.com](https://gitlab.com/lixie-fun/lixie-fun)**
---

![lixie-title](lixie_fun.jpg)

**Content:**  

[[_TOC_]]

# What is a Lixie?
A Lixie display is a new take on [Nixie-Tubes](https://en.wikipedia.org/wiki/Nixie_tube) that were introduced in the 1950s and only produced until the 90s when cheaper displays took over. Nixie tubes consist of a stack of wires or thin tin in the shape of all numbers from 0 to 9. Applying power to them emits light and by activating one number in the stack, only one of them glows. To enhance visibility, they were often placed inside gas-filled tubes: hence the name Nixie-Tube.  
Lixies on the other hand don't have glowing wires but rather engraved acrylic glass panes with LEDs shinig light through the edges. Since the panes don't touch each other, it's possible to have only one of them glowing at a time. Lixies have a very reminiscing aesthetic, and due to rgb-LEDs they can be more individual than nixies by adjusting colour and brightness.  

![lixie](Hardware/case/doc/lixie_principle.png)

Since we need something for the display should show, we focused on building a clock that works the same way as a typical radio clock. However, the display is designed so that it can be reconfigured for other purposes as well.

# DCF77
Getting the current time could be done over the internet, using the _Network Time Protocol_ (NTP) for example, but this tends to get quite complicated, since it would involve a Wi-Fi chip along with several big libraries to get it going, and more input from the user to get it connected to the internet.  
Instead, we decided to use a [DCF77](https://en.wikipedia.org/wiki/DCF77)-Antenna. DCF77 is a 77,5 kHz time signal broadcast from Frankfurt, Germany accross large parts of Europe. The signal is transmitting 59 bits every minute, containing time and date, as well as weather data (proprietary encoding).

## dcf77_sim
For the early development phases, it was helpful to have a way to emulate an ideal DCF77 signal by playing a pre-defined bit array.  
This is just a small Arduino sketch that can be loaded onto a second device to be used in the circuit instead of the actual DCF77 antenna.  
Since it isn't really part of the Lixie clock, we decided to outsource it to [its own project](https://gitlab.com/BadWolf/dcf77-simulator).

# Content of this Repository
* **[Hardware/case](Hardware/case/)**  
  Files for 3D-printing the cases  
* **[Hardware/led_pcb](Hardware/led_pcb/)**  
  Schematics and gerber files of the segment PCBs
* **[Hardware/control_circuit](Hardware/control_circuit/)**  
  The main circuit connecting all parts  
* **[Hardware/numeral_plates](Hardware/numeral_plates/)**  
  Files of the numeral plates  
* **[Code/](Code/)**  
  Code of the Lixie clock  

# Parts and Materials
* 1x Controller with _Atmega328p_ chip, or compatible (For example *Arduino Uno*/*Nano*)
* 1x [DS3231 RTC](https://www.amazon.de/dp/B01M2B7HQB/ref=twister_B07ZQGBH14?_encoding=UTF8&psc=1) Module
* 1x [Pollin DCF1](https://www.pollin.de/p/dcf-77-empfangsmodul-dcf1-810054) Module
* 1x Rotary Potentoiometer >= 100 kΩ
* 120x SK9822 or APA102 SMD RGB LED
* 6x Custom PCB (see [PCB schematics](Hardware/led_pcb/))
* 6x pin header 1x4 male, 90°
* 7x pin header 1x4 female, 90°
* 2x pin header 1x4 female
* 1x pin header 1x3 female
* ~ 16m of 3D printer filament (dark colors recommended - less light leakage)
* 380mm x 780mm PMMA acrylic glass

The total cost of all parts is probably around **50€**, but can largely depend on where, or what variants of the parts are bought (20$ Arduino vs. 3$ Chinese clone), shipping, etc.

The following tools were used: 
* Soldering Iron
* 3D Printer (We used a [_Prusa i3 MK3S_](https://www.prusa3d.com/original-prusa-i3-mk3/))
* Lasercutter (We used a [_BRM Original 100160_](https://www.brmlasers.com/laser-machines/original-lasers/brm-100160/))

# Hardware Setup
The Lixie clock is made of two main components:  

* The control circuit
* The Lixie display

The display consists of six modularly designed digit segments:  

![displaysegment](Hardware/case/doc/segment.png)  

That way, any number of segments can be chained for other use cases. (The only limit here really is the power the Arduino can provide. If that is exceeded, an external 5V power supply needs to be used.)  
Each segment block has the input side on the left (male connector), and the output side on the right (female connector). The control circuit is connected to the input side of the first segment.


## Assembly
The components need to be connected according to the following circuit diagram:  

![circuit diagram](Hardware/control_circuit/doc/circuit_diagram.jpg)

A casing, as well as another custom PCB for the controller, is planned/work-in-progress. The controller case can be found on the [respective branch](https://gitlab.mi.hdm-stuttgart.de/embedded/ws19-20/lixie-fun/-/tree/77-controller-gehause-design/3D-Printing/10PanelSegment).  

For now, the components can be soldered to a circuit board,  
![holegrid setup](Hardware/control_circuit/doc/circuit_board.jpg)  

... or connected on a breadboard.  
![breadboard setup](Hardware/control_circuit/doc/breadboard.jpg)  

Notes:
* **Important!** The pins on the antenna PCB are not labelled correctly! (See [Arduino Forum post](https://www.arduinoforum.de/arduino-Thread-Hinweise-zum-DCF77-Modul-u-a-von-Pollin))  
![Antenna-Pins](Hardware/control_circuit/doc/antenna_pins.jpg)
* The single LED in the plans represents the input of the first display segment.  
* The ideal signal-to-noise ratio is achieved by carefully adjusting the signal processing potentiometer (R4) to a value of about 3,04 kΩ. This value depends on the used transistor.

## Arduino IO ports
| Usage | Port | Configurable |
|-----------------------------------------------------------------------------|------|--------------|
| Ground for all power using devices | GND | No |
| Power for LEDs and DS3231 RTC module, reference voltage for brightness poti | 5V | No |
| Power for DCF1 module | 3.3V | No |
| Data input (brightness controller potentiometer) | A0 | Yes* |
| Data line (RTC module) | A4 | No** |
| Clock line (RTC module) | A5 | No** |
| Data input (processed signal from DCF antenna) | D2 |  Yes*** |
| Data line (LED chain) | D11 | No** |
| Clock line (LED chain) | D13 | No** |

\* Only analog inputs can be used. Adapt the channel parameter of `adc_read()` accordingly.  
\** These ports cannot be changed on the Atmega328P. Other controllers might allow configuration or have these functions fixed on a different port.  
\*** Only digital inputs can be used. The `INT0` interrupt is tied to the IO pin. Change the interrupt vector accordingly.

## Uploading the program
Requirements:
* [avr toolchain](https://www.microchip.com/mplab/avr-support/avr-and-arm-toolchains-c-compilers) (login required)
* [avrdude](https://www.nongnu.org/avrdude/)
* [make](http://www.gnu.org/software/make/)

In order to upload the program to the controller:  
1. Use a command line interface to navigate to the code directory `Code/`.
2. Run `make program`   
   * For MacOS, the port name `/dev/tty.usbmodem*` is set as default.   
   * On operating systems other than MacOS, run `make program PORT=<port>`, where `<port>` stands for the port name through which the Arduino is connected to the computer.  
   On Windows, this is usually a COM port, which can be found out in the device manager.  
   On Linux, it is likely a `/dev/ttyACM*` or `/dev/ttyUSB*` device. The exact name can be found out by comparing `ls /dev/` with and without the device plugged in.  

_Note: We used avr-gcc version 9.2:_
```
$ avr-gcc --version
avr-gcc (Fedora 9.2.0-6.fc33) 9.2.0
```
_Upgrading to version 10.2.0 led to the project not building any more for some reason._

# Setup and Usage
* The clock requires a USB power supply. 
* For ideal reception, the DCF antenna should be aligned in a 90°-ish angle towards the DCF77 transmitter in Mainflingen near Frankfurt am Main.  
  ```
  Frankfurt
    ^
    |
  --==- Antenna
  ```
* After receiving power, the clock will start to read the DCF77 radio signal while showing a count-up on the display. Once the start of a new minute was recognized, and the following 59 bits were received without error, the clock will start to show the current time.
* Twice a minute, on the seconds 10-13 and 40-43, the clock shows the current date.
* Twice a minute, on the seconds 20-23 and 50-53, the clock shows the current temperature.
* The dial switch can be used to dim the LED brightness. The lowest setting equals a brightness of zero, effectively turning the LEDs off.
